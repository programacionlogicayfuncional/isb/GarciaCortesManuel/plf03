(ns plf03.core)

(defn funcion-comp-1
  []
  (let [
        f (fn [x] (* x -1))
        g (fn [x] (dec x))
       z (comp f g) ]
      (z 10)
    ))
(defn funcion-comp-2
  []
  (let [
        f (fn [x y] (+ x y))
        g (fn [x] (* x x))
        z (comp f g)]
    (z 2 7)
    ))
(defn funcion-comp-3
  []
  (let [
        f (fn [xs] (even? xs))
        g (fn [xs] (first xs))
        z (comp f g)]
    (z #{10 16 15 20 7 9 3 2})
    ))
(defn funcion-comp-4
  []
  (let [
        f (fn [xy xs] (list xy xs ))
        g (fn [xs] (last xs))
        h (fn [xy] (first xy))
        z (comp g h f)]
    (z [1 2 3] [5 7 9])))

(defn funcion-comp-5
  []
  (let [
        f (fn [x y ] (- x y))
        g (fn [x] (inc x))
        z (comp g f)]
    (z 10 20)))
(defn funcion-comp-6
  []
  (let [
        f (fn [xs] (map count xs))
        g (fn [xs ys] (vector xs ys))
        z (comp f g)]
    (z [1 3 5] [0 9 6])))
(defn funcion-comp-7
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (vector? xs))
        z (comp g f)]
    (z {:a 1 :b 2})
    ))
(defn funcion-comp-8
  []
  (let [
        f (fn [x y u] (range x y u))
        g (fn [x] (map even? x))
        z (comp g f)]
    (z 1 10 2)))
(defn funcion-comp-9
  []
  (let [
        f (fn [xs x] (take-last x xs))
        g (fn [xs] (filter even? xs))
        h (fn [xs] (count xs))
        z (comp  h g f)
        ]
    (z [10 15 25 45 55 60 65 80] 3)))

(defn funcion-comp-10
  []
  (let [
        f (fn [x] (inc x))
        g (fn [y] (dec y))
        h (fn [x y] (* x y))
        z (comp  f g h)]
    (z 10 15)))

(defn funcion-comp-11
  []
  (let [
        f (fn [xs] (filter coll? xs))
        g (fn [x] (reverse x))
        h (fn [x xs] (list x xs))
        z (comp f g h)]
    (z 10 [-2 4 -6 8 -10 12 -14])))



(defn funcion-comp-12
  []
  (let [
        f (fn [x xs] (take x xs))
        g (fn [x] (sequential? x))
        h (fn [xs] (true? xs) )
        z (comp h g f)]
    (z 2 [1 2 3 4 5 6 7 8 9 0])))

(defn funcion-comp-13
  []
  (let [
        f (fn [x] (first x))
        g (fn [x xs] (concat x xs))
        h (fn [xs] (char? xs))
        z (comp h f g)]
    (z "123" [0 9 2])))

(defn funcion-comp-14
  []
  (let [
        f (fn [xs] (reverse xs))
        g (fn [x xs] (take-last x xs))
        h (fn [x] (filter pos? x))
        z (comp f h g)]
    (z 4 [-15 10 -5 0 20 -25 30 -100 33])))

(defn funcion-comp-15
  []
  (let [f (fn [x] (filter char? x))
        g (fn [x xs] (concat x xs))
        h (fn [xs] (count xs))
        z (comp h f g)
        ]
    (z "hola" [1 2 3 \j \i])))

(defn funcion-comp-16
  []
  (let [f (fn [x y] (range x y))
        g (fn [x] (inc x))
        h (fn [y] (count y))
        z(comp g h f) ]
    (z 2 20)))
(defn funcion-comp-17
  []
  (let [f (fn [xs ys] (into xs ys))
        g (fn [xs] (last xs))
        h (fn [ys] (vector? ys))
       z(comp h g f) ]
    (z [] {:a 2 :b 4 :c 8})))

(defn funcion-comp-18
  []
  (let [f (fn [x y w] (- x y w))
        g (fn [x] (pos? x)) 
        h (fn [w] (* -1 w))
        i (fn [y] (inc y))
        z (comp g h i f)]
    (z 10 20 30)))

(defn funcion-comp-19
  []
  (let [f (fn [x xs] (conj x xs))
        g (fn [xs] (take-last 2 xs))
        h (fn [x] (reverse x))
      z(comp g h f)]
    (z [2 5 7] 10)))

(defn funcion-comp-20
  []
  (let [f (fn [x] (count x))
        g(fn [xs ys] (conj xs ys))
        h (fn [xs] (into '() xs))
        i(fn [ys] (dec ys))
        z(comp  i f h g)]
    (z  [1 2 3][4 5 6])))

(funcion-comp-1)
(funcion-comp-2)
(funcion-comp-3)
(funcion-comp-4)
(funcion-comp-5)
(funcion-comp-6)
(funcion-comp-7)
(funcion-comp-8)
(funcion-comp-9)
(funcion-comp-10)
(funcion-comp-11)
(funcion-comp-12)
(funcion-comp-13)
(funcion-comp-14)
(funcion-comp-15)
(funcion-comp-16)
(funcion-comp-17)
(funcion-comp-18)
(funcion-comp-19)
(funcion-comp-20)

(defn funcion-complement-1
  []
  (let [
        f (fn [x] (pos? x))
        z (complement f)]
     (z -10)))

(defn funcion-complement-2
  []
  (let [ f(fn [x] (number? x))
        z (complement f)]
    (z "1")))

(defn funcion-complement-3
  []
  (let [ f (fn [x] (char? x))
         z (complement f)]
    (z \a)))
(defn funcion-complement-4
  []
  (let [f (fn [xs] (coll? xs))
        z (complement f)]
    (z #{1 23 2})))

(defn funcion-complement-5
  []
  (let [f (fn [xs] (sorted? xs))
        z (complement f)]
    (z #{2 4 5})))

(defn funcion-complement-6
  []
  (let [ f(fn [xs] (sequential? xs))
         z (complement f)]
    (z [1 2 3 4])))

(defn funcion-complement-7
  []
  (let [ f (fn [xs] (associative? xs))
         z (complement f)]
    (z {:a 1 :b 2})))
(defn funcion-complement-8
  []
  (let [f (fn [x y] (identical? x y))
        z (complement f)]
    (z 10 15)))

(defn funcion-complement-9
  []
  (let [f (fn[xs] (nil? xs))
        z (complement f)]
    (z nil)))

(defn funcion-complement-10
  []
  (let [f (fn[xs] (some? xs))
        z (complement f)]
    (z [])))

(defn funcion-complement-11
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (number? xs))
        h (comp g f)
        z (complement h)]
    (z [1 2 3])))

(defn funcion-complement-12
  []
  (let [f (fn[xs] (count xs))
        g (fn[xs] (zero? xs))
        h (comp g f)
        z (complement h)]
    (z [2 3])))

(defn funcion-complement-13
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (dec x))
        h (comp f g)
        z (complement h)]
    (z 8)))

(defn funcion-complement-14
  []
  (let [f (fn [x] (zero? x))
        g (fn [x] (- x 10))
        h (fn [x] (/ x 2))
        i (comp f g h)
        z (complement i)]
    (z 12)))

(defn funcion-complement-15
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (* 3 x))
        h (fn [x] (+ 5 x))
        i (comp f g h)
        z (complement i)]
    (z 1)))

(defn funcion-complement-16
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (vector? xs))
        h (comp g f)
        z (complement h)]
    (z [[1 2][3 4][5 6]])))
(defn funcion-complement-17
  []
  (let [f (fn [xs] (sorted? xs))
        g (fn [xs] (reverse xs))
        h (fn [xs] (take 3 xs))
        i (comp f g h)
        z (complement i)]
    (z [9 0 3 4 5])))
(defn funcion-complement-18
  []
  (let [f (fn [xs] (sequential? xs))
        g (fn [xs] (conj xs 3))
        h (fn [xs] (take-last 2 xs))
        i (comp f h g)
        z (complement i)]
    (z [10 12 11 19])))
(defn funcion-complement-19
  []
  (let [f (fn [xs] into '() xs)
        g (fn [xs] (last xs))
        h (fn [xs] (coll? xs))
        i (comp h g f)
        z (complement i )]
    (z [2 3 9 7])))

(defn funcion-complement-20
  []
  (let [f (fn [x] (range x 100))
        g (fn [x] (coll? x))
        h (comp g f)
        z (complement h)]
    (h -2)))




(funcion-complement-1)
(funcion-complement-2)
(funcion-complement-3)
(funcion-complement-4)
(funcion-complement-5)
(funcion-complement-6)
(funcion-complement-7)
(funcion-complement-8)
(funcion-complement-9)
(funcion-complement-10)
(funcion-complement-11)
(funcion-complement-12)
(funcion-complement-13)
(funcion-complement-14)
(funcion-complement-15)
(funcion-complement-16)
(funcion-complement-17)
(funcion-complement-18)
(funcion-complement-19)
(funcion-complement-20)

(defn funcion-constantly-1
  []
  (let [f (fn [x] (* x 3))
        z (constantly f)]
    (z 10)))

(defn funcion-constantly-2
  []
  (let [f (fn [x] (+ 2 x))
        z (constantly f)]
    (z 12)))

(defn funcion-constantly-3
  []
  (let [f (fn[x] (* 0 x))
        z (constantly f)]
    (z 3)))

(defn funcion-constantly-4
  []
  (let [f (fn [x] (pos? x))
        z (constantly f)]
    (z -2)))

(defn funcion-constantly-5
  []
  (let [f (fn [x] (zero? x))
        z (constantly f)]
    (z 0)))
(defn funcion-constantly-6
  []
  (let [f (fn [x] (neg? x))
        z (constantly f)]
    (z 9)))

(defn funcion-constantly-7
  []
  (let [f (fn [xs] (first xs))
        z (constantly f)]
    (z '(2 4 6 8))))
(defn funcion-constantly-8
  []
  (let [f (fn [xs] (last xs))
        z (constantly f)]
    (z {:a 1 :b 2 :c 3 :d 4})))
(defn funcion-constantly-9 
  []
  (let [f (fn [xs] (vector? xs))
        z (constantly f)]
    (z [0 1 0 1])))
(defn funcion-constantly-10
  []
  (let [f (fn[xs] (list? xs))
        z (constantly f)]
    (z '(0 2 4 9))))
(defn funcion-constantly-11
  []
  (let [f (fn [x] (+ 1 x))
        g (fn [x] (* 2 x))
        h (comp f g)
        z (constantly h)]
    (z 7)))
(defn funcion-constantly-12
  []
  (let [f (fn [x] (dec x))
        g (fn [x] (/ x 10))
        h (comp g f)
        z (constantly h)]
    (z 5)))
(defn funcion-constantly-13
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (* x x))
        h (comp f g)
        z (constantly h)]
    (z)))
(defn funcion-constantly-14
  []
  (let [f (fn[x] (odd? x))
        g (fn[x] (/ x 1))
        h (comp f g)
        z (constantly h)]
    (z 40)))
(defn funcion-constantly-15
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (inc xs))
        h (comp g f)
        z (constantly h)]
    (z '(9 0 3 4))))
(defn funcion-constantly-16
  []
  (let [f (fn [xs] (last xs))
        g (fn [xs] (dec xs))
        h (comp g f)
        z (constantly h)]
    (z #{3 5 6 0})))
(defn funcion-constantly-17
  []
  (let [f (fn [xs] (filter even? xs))
        g (fn [xs] (count xs))
        h (comp g f)
        z (constantly h)]
  (z [1 2 3 4 5 6 7 8])))
(defn funcion-constantly-18
  []
  (let [f (fn [xs] (filter odd? xs))
        g (fn [xs] (reverse xs))
        h (comp g f)
        z (constantly h)]
    (z '(10 11 12 13 14 15 16 17 18))))

(defn funcion-constantly-19
  []
  (let [f (fn [xs] (filter coll? xs))
        g (fn [xs] (take 2 xs))
        h (comp g f)
        z (constantly h)]
    (z [#{1 2} [3 4] '(5 6) {} 7 8])))
(defn funcion-constantly-20
  []
  (let [f (fn [xs] (filter sequential? xs))
        g (fn [xs] (take-last 3 xs))
        h (comp g f)
        z (constantly h)]
   (z [#{1 2} [3 4] '(5 6) {} 7 8])))

(funcion-constantly-1)
(funcion-constantly-2)
(funcion-constantly-3)
(funcion-constantly-4)
(funcion-constantly-5)
(funcion-constantly-6)
(funcion-constantly-7)
(funcion-constantly-8)
(funcion-constantly-9)
(funcion-constantly-10)
(funcion-constantly-11)
(funcion-constantly-12)
(funcion-constantly-13)
(funcion-constantly-14)
(funcion-constantly-15)
(funcion-constantly-16)
(funcion-constantly-17)
(funcion-constantly-18)
(funcion-constantly-19)
(funcion-constantly-20)

(defn funcion-every-pred-1
  []
  (let [f (fn [x] (odd? x))
        g (fn [xs] (coll? xs))
        h (fn [x xs] (contains? xs x))
        z (every-pred f g h)]
    (z 1 [1 3 4])))

(defn funcion-every-pred-2
  []
  (let [f (fn [x] (odd? x))
        g (fn [x] (number? x))
        z (every-pred g f)]
    (z 3)))

(defn funcion-every-pred-3
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (> x 10))
        h (fn [x] (< x 20))
        z (every-pred h g f)]
    (z 12 10)))

(defn funcion-every-pred-4
  []
  (let [f (fn [x] (== x 2))
        g (fn [y] (== y 4))
        z (every-pred f g)]
    (z 4 2)))
(defn funcion-every-pred-5
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (even? x))
        z (every-pred g f)]
    (z 20)))
(defn funcion-every-pred-6
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (coll? xs))
        z (every-pred g f)]
    (z [2 3 4 5])))
(defn funcion-every-pred-7
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (sequential? xs))
        z (every-pred g f)]
    (z '(10 12 14 16 20))))
(defn funcion-every-pred-8
  []
  (let [f (fn [xs] (sorted? xs))
        g (fn [xs] (set? xs))
        z (every-pred g f)]
    (z #{2 3 4})))
(defn funcion-every-pred-9
  []
  (let [f (fn [xs] (coll? xs))
        g (fn [xs] (list? xs))
        z (every-pred f g)]
    (z '(\a \b \c))))

(defn funcion-every-pred-10
  []
  (let [f (fn [xs] (sorted? xs))
        g (fn [xs] (list? xs))
        z (every-pred f g)]
    (z [1 2 4])))

(defn funcion-every-pred-11
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (neg? y))
        h (fn [w] (zero? w))
        z (every-pred f g h)]
    (z 10 20 0)))
(defn funcion-every-pred-12
  []
  (let [f (fn [x] (number? x))
        g (fn [y] (pos-int? y))
        z (every-pred g f)]
    (z 1 2)))
(defn funcion-every-pred-13
  []
  (let [f (fn [xs] (set? xs))
        z (every-pred f)]
    (z {2 6 8 12})))
(defn funcion-every-pred-14
  []
  (let [f (fn [xs] (vector? xs))
        z (every-pred f)]
    (z [2 3])))
(defn funcion-every-pred-15
  []
  (let [f (fn [xs] (list? xs))
        z (every-pred f)]
    (z '( 9 0))))
(defn funcion-every-pred-16
  []
  (let [f (fn [xs] (coll? xs))
       z (every-pred f)]
    (z {})))
(defn funcion-every-pred-17
  []
  (let [f (fn [xs] (some? xs))
        z (every-pred f)]
    (z [])))
(defn funcion-every-pred-18
  []
  (let [f (fn [xs] (nil? xs))
        z (every-pred f)]
    (z nil)))
(defn funcion-every-pred-19
  []
  (let [f (fn [xs] (empty? xs))
        z (every-pred f)]
    (z '())))
(defn funcion-every-pred-20
  []
  (let [f (fn [xs] (boolean? xs))
        z (every-pred f)]
    (z '(true false))))

(funcion-every-pred-1)
(funcion-every-pred-2)
(funcion-every-pred-3)
(funcion-every-pred-4)
(funcion-every-pred-5)
(funcion-every-pred-6)
(funcion-every-pred-7)
(funcion-every-pred-8)
(funcion-every-pred-9)
(funcion-every-pred-10)
(funcion-every-pred-11)
(funcion-every-pred-12)
(funcion-every-pred-13)
(funcion-every-pred-14)
(funcion-every-pred-15)
(funcion-every-pred-16)
(funcion-every-pred-17)
(funcion-every-pred-18)
(funcion-every-pred-19)
(funcion-every-pred-20)

(defn funcion-fnil-1
  []
  (let [f (fn [x y] (if (> x y) (+ x y) (- x y)))
        z (fnil f 50)]
    (z nil 10)))
(defn funcion-fnil-2
  []
  (let [
        i (fn [x y] (if (neg? x) (inc x) (* y x)))
        z (fnil i 2)]
    (z nil 10))) 

(defn funcion-fnil-3
  []
  (let [f (fn [xs x] (if (pos? x) (vector xs x) (vector )))
        z (fnil f [4 5 7])]
    (z nil 9)))

(defn funcion-fnil-4
  []
  (let [f (fn [x y] ( if (== x y) (str x "1") (str x 3)))
        z (fnil f 10)]
    (z nil 10)))

(defn funcion-fnil-5
  []
  (let [f (fn [xs x] (if (sequential? xs) (conj xs x) (reverse xs)))
        z (fnil f #{3 4 7})]
    (z nil 2)))

(defn funcion-fnil-6
  []
  (let [
        f (fn [xs x] (if (contains? xs x) (first xs) (last xs)))
        z(fnil  f [9 8 7])]
    (z nil 0)))

(defn funcion-fnil-7
  []
  (let [f (fn [xs ys] (if (sequential? ys) (into [] ys) (into []xs)))
        z (fnil f {:a :b})]
    (z nil '("b" \$ 1))))

(defn funcion-fnil-8
  []
  (let [f (fn [x xs] (if (sorted? xs) (list* x) (list* xs)))
        z (fnil f 3)]
    (z nil [2 3 1 4])))

(defn funcion-fnil-9
  []
  (let [f (fn [x xs] (if (vector? xs) (filter number? xs) (range x)))
        z (fnil f 10)]
    (z nil [20 30 40 \a \b])))
(defn funcion-fnil-10
  []
  (let [f (fn [x xs] (if (list? xs) (group-by char? xs) (dec x)))
        z (fnil f 0)]
    (z nil '(\a \b))))

(defn funcion-fnil-11
  []
  (let [f (fn [xs x] (if (zero? x) (concat xs x) (inc x)))
        z (fnil f [\a \b \c])]
    (z nil 1)))

(defn funcion-fnil-12
  []
  (let [f (fn [xs ys] (if (empty? ys) (empty xs) (into xs ys)))
        z (fnil f '())]
    (z nil [3 4 5])))

(defn funcion-fnil-13
  []
  (let [ f (fn [xs st] (if (string? st) (str xs st) (str st xs)))
        z (fnil f [1 1 0 0])]
    (z nil "0011")))

(defn funcion-fnil-14
  []
  (let [f (fn [x y] (if (< x y) (range x y) (range y x)))
        z (fnil f 1)]
    (z nil 9)))
(defn funcion-fnil-15
  []
  (let [f(fn [xs x] (if (nil? x) (take 2 xs) (take 1 xs)))
        z (fnil f [9 8 7])]
    (z nil 0))
  )
(defn funcion-fnil-16
  []
  (let [ f (fn [xs ys] (if (every? number? ys) (conj xs ys) (concat xs ys)))
        z (fnil f [0 9 7])]
    (z nil [10 12 14])))
(defn funcion-fnil-17
  []
  (let [ f (fn [xs ys] (if (set? ys) (reverse xs) (take-last 2 xs)))
        z (fnil f #{\a \b \c 15 30 45})]
    (z nil {})))

(defn funcion-fnil-18
  []
  (let [ f (fn [xs ys] (coll? ys) (vector xs ys) (list xs ys))
        z (fnil f [12])]
    (z nil [2 5 8]))) 

(defn funcion-fnil-19
  []
  (let [f (fn [xs x] (if (char? x) (hash-set xs) (hash-map xs x)))
        z (fnil f [[:a 1] [:b 2] [:c 3]])]
    (z nil "a")))

(defn funcion-fnil-20
  []
  (let [f (fn [xs x] (if (contains? xs x) (conj xs x) (pop xs)))
        z (fnil f [2 17 18])]
    (z  nil 0)))

(funcion-fnil-1)
(funcion-fnil-2)
(funcion-fnil-3)
(funcion-fnil-4)
(funcion-fnil-5)
(funcion-fnil-6)
(funcion-fnil-7)
(funcion-fnil-8)
(funcion-fnil-9)
(funcion-fnil-10)
(funcion-fnil-11)
(funcion-fnil-12)
(funcion-fnil-13)
(funcion-fnil-14)
(funcion-fnil-15)
(funcion-fnil-16)
(funcion-fnil-17)
(funcion-fnil-18)
(funcion-fnil-19)
(funcion-fnil-20)

(defn funcion-juxt-1
  []
  (let [f (fn [x] (* 4 x))
        z (juxt f)]
    (z 5)))
(defn funcion-juxt-2
  []
  (let [f (fn [x] (+ 5 x))
        g (fn [x] (/ x 2))
        z (juxt f g)]
    (z 1)))

(defn funcion-juxt-3
  []
  (let [f (fn [x y] (+ x y))
        g (fn [x y] (== x y))
        z (juxt g f)]
    (z 10 20)))
(defn funcion-juxt-4
  []
  (let [f (fn [x y w] (* x y w))
        g (fn [x y w] (conj x w y))
        h (fn [x y w] (> x y w))
        z (juxt f g h)]
    (z 9 7 8)))
(defn funcion-juxt-5
  []
  (let [f (fn [x y w] (range x y w))
        g (fn [x y w] (str x y w))
        h (fn [x y w] (< x y w))
        z (juxt h g f)]
    (z 3 20 5)))
(defn funcion-juxt-6
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (last xs))
        z (juxt f g)]
    (z '(0 2 30 98 90 3 2 45 54))))
(defn funcion-juxt-7
  []
  (let [f (fn [xs] (take 2 xs))
        g (fn [xs] (take-last 2 xs))
       z (juxt g f) ]
    (z [0 2 3 0 5 6  5 7 9 3 4 5])))
(defn funcion-juxt-8
  []
  (let [f (fn [xs] (sequential? xs))
        g (fn [xs] (sorted? xs))
        z (juxt f g)]
    (z [1 2 3 4])))
(defn funcion-juxt-9
  []
  (let [f (fn [xs] (filter even? xs))
        g (fn [xs] (filter odd? xs))
        z (juxt g f)]
    (z [0 1 2 3 4 5 6 7 8 9 10 11 12])))
(defn funcion-juxt-10
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (list? xs))
        z (juxt g f)]
    (z '())))

(defn funcion-juxt-11
  []
  (let [f (fn [xs ys] (concat xs ys))
        g (fn [xs ys] (str xs ys))
        z (juxt f g)]
    (z '(\a \e \i \o \u) [0 1 2 3 4])))

(defn funcion-juxt-12
  []
  (let [f (fn [xs ys] (conj xs ys))
        g (fn [xs ys] (conj ys xs))
        z (juxt g f)]
    (z #{1 9 0} #{3 27})))

(defn funcion-juxt-13
  []
  (let [f (fn [xs ys] (vector xs ys))
        g (fn [xs ys] (list xs ys))
        z( juxt f g)]
    (z [2 4 7] [9 12 16])))

(defn funcion-juxt-14
  []
  (let [f (fn [xs ys] (hash-set xs ys)) 
        g (fn [xs ys] (sorted-set xs ys))
        z (juxt g f)]
    (z '(2 4 10) [12 14 21])))
(defn funcion-juxt-15
  []
  (let [f (fn [xs ys] (conj xs ys))
        g (fn [xs] (reverse xs))
        h (comp g f)
        z (juxt h)]
    (z [10 15 4] [12 14 18])))

(defn funcion-juxt-16
  []
  (let [f (fn [xs ys] (mapv + xs ys))
        g (fn [xs ys] (mapv - xs ys))
        z (juxt f g)]
    (z [10 20 3] [1 2 30])))

(defn funcion-juxt-17
  []
  (let [f (fn [xs] (filterv number? xs))
        g (fn [ys] (filterv char? ys))
        h (fn [zs] (filterv symbol? zs))
        z (juxt g h f)]
    (z [\v \w] [\% \#] [1 2] )))

(defn funcion-juxt-18
  []
  (let [f (fn [xs x] (list* xs x))
        z (juxt f)]
    (z 1 '(5 8 3))))

(defn funcion-juxt-19
  []
  (let [f (fn [xs ys zs] (conj xs ys zs))
        g (fn [xs ys zs] (str xs ys zs))
        h (fn [xs ys zs] (cons xs ys zs))
        z (juxt h g f)]
    (z [\a \b] [1 2] [])))

(defn funcion-juxt-20
  []
  (let [ f (fn [xs] (group-by char? xs))
         g (fn [ys] (group-by number? ys))
        h (fn [zs] (group-by pos? zs))
        z (juxt f g h)]
    (z [\A 1 \B 2] [\C 3 \D 4] [9 -1 2])))

(funcion-juxt-1)
(funcion-juxt-2)
(funcion-juxt-3)
(funcion-juxt-4)
(funcion-juxt-5)
(funcion-juxt-6)
(funcion-juxt-7)
(funcion-juxt-8)
(funcion-juxt-9)
(funcion-juxt-10)
(funcion-juxt-11)
(funcion-juxt-12)
(funcion-juxt-13)
(funcion-juxt-14)
(funcion-juxt-15)
(funcion-juxt-16)
(funcion-juxt-17)
(funcion-juxt-18)
(funcion-juxt-19)
(funcion-juxt-20)

(defn funcion-partial-1
  []
  (let [ f (fn [x y] (+ 3 x y))
      z (partial f)]
 (z 15 10) ))


(defn funcion-partial-2
  []
  (let [f (fn [x y] (== x y))
        z (partial f)]
    (z 3 2)))

(defn funcion-partial-3
  []
  (let [f (fn [x y z] (> x z z))
        z (partial f 1)]
    (z 2 3)))


(defn funcion-partial-4
  []
  (let [f (fn [x y z] (< x z z))
        z (partial f 6)]
    (z 5 4)))

(defn funcion-partial-5
  []
  (let [f (fn [x y z] (* x y z))
        z (partial f 0)]
    (z 9 8)))
(defn funcion-partial-6
  []
  (let [f (fn [x] (inc x))
        z (partial f)]
    (z 9)))

(defn funcion-partial-7
  []
  (let [f (fn [x] (dec x))
        z (partial f)]
    (z 11)))

(defn funcion-partial-8
  []
  (let [f (fn [x] (pos? x))
        z (partial f)]
    (z 1)))
(defn funcion-partial-9
  []
  (let [f (fn [x] (neg? x))
        z (partial f)]
    (z -1)))
(defn funcion-partial-10
 []
  (let [f (fn [x] (zero? x))
        z (partial f)]))
(defn funcion-partial-11
  []
  (let [f (fn [x xs] (contains? xs x))
        z (partial f 1)]
    (z [2 4 7])))

(defn funcion-partial-12
  []
  (let [f (fn [x xs] (str xs x))
        z (partial f 3)]
    (z '(2 4 5))))

(defn funcion-partial-13
  []
  (let [f (fn [xs x] (into xs x))
        z (partial f 4)]
    (z [])))

(defn funcion-partial-14 
  []
  (let [f (fn [xs x] (hash-set xs x))
        z (partial f 1)]
    (z [2 4])))

(defn funcion-partial-15
  []
  (let [f (fn [xs x] (list xs x))
        z (partial f 3)]
    (z '(2 5 9))))

(defn funcion-partial-16
  []
  (let [f (fn [xs ys] (vector xs ys))
        z (partial f [2 4]) ]
    (z [0 1])))

(defn funcion-partial-17
  []
  (let [f (fn [xs ys] (conj xs ys))
        z (partial f '())]
    (z #{10 20})))
(defn funcion-partial-18
  []
  (let [f (fn [st1 st2] (concat st1 st2))
        z (partial f "hola")]
    (z "manuel")))
(defn funcion-partial-19
  []
  (let [f (fn [x y z] (range x y z))
        z (partial f 1 100)]
    (z 10)))
(defn funcion-partial-20
  []
  (let [f (fn [xs ys] (cons xs ys))
        z (partial f '(2 4))]
    (z [5 6])))

(funcion-partial-1)
(funcion-partial-2)
(funcion-partial-3)
(funcion-partial-4)
(funcion-partial-5)
(funcion-partial-6)
(funcion-partial-7)
(funcion-partial-8)
(funcion-partial-9)
(funcion-partial-10)
(funcion-partial-11)
(funcion-partial-12)
(funcion-partial-13)
(funcion-partial-14)
(funcion-partial-15)
(funcion-partial-16)
(funcion-partial-17)
(funcion-partial-18)
(funcion-partial-19)
(funcion-partial-20)

(defn funcion-some-fn-1
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (neg? x))
        z (some-fn f g)]
    (z -10)))
(defn funcion-some-fn-2
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (zero? x))
        z (some-fn f g)]
    (z 7)))

(defn funcion-some-fn-3
  []
  (let [f (fn [x] (odd? x))
        g (fn [x] (int? x))
        z (some-fn f g)]
    (z 10)))

(defn funcion-some-fn-4
  []
  (let [f (fn [x] (double? x))
        g (fn [x] (float? x))
        z (some-fn f g)]
    (z 1.9)))

(defn funcion-some-fn-5
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (char? x))
        z (some-fn g f)]

    (z 2)))

(defn funcion-some-fn-6
  []
  (let [f (fn [x y] (> x y))
        g (fn [x y] (< x y))
        h (fn [x y] (== x y))
        z (some-fn h g f)]
    (z 12 24 )))

(defn funcion-some-fn-7
  []
  (let [f (fn [b] (true? b))
        g (fn [b] (false? b))
        z (some-fn g f)]
    (z true)))
(defn funcion-some-fn-8
  []
  (let [f (fn [x] (nil? x))
        g (fn [x] (some? x))
        z (some-fn f g)]
    (z nil)))
(defn funcion-some-fn-9
  []
  (let [f (fn [x] (> x 100))
        g (fn [x] (< x 100))
        z (some-fn g f)]
    (z 30)))
(defn funcion-some-fn-10
  []
  (let [f (fn [x] (> x 30))
        g (fn [x] (< x 50))
        z (some-fn g f)]
    (z 15)))
(defn funcion-some-fn-11
  []
  (let [f (fn [xs] (coll? xs))
        g (fn [xs] (sequential? xs))
        z (some-fn f g)]
    (z [0 2])))
(defn funcion-some-fn-12
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (list? xs))
        z (some-fn g f)]
    (z [2 4 5])))
(defn funcion-some-fn-13
  []
  (let [f (fn [xs] (set? xs))
        g (fn [xs] (map? xs))
        z (some-fn f g)]
    (z #{2 4})))
(defn funcion-some-fn-14
  []
  (let [f (fn[xs] (empty? xs))
        g (fn[xs] (some? xs))
        z (some-fn f g)]
 (z [])   ))
(defn funcion-some-fn-15
  []
  (let [f (fn [xs] (sorted? xs ))
        g (fn [xs] (associative? xs))
        z (some-fn f g)]
    (z {1 2 3 4})))
(defn funcion-some-fn-16
  []
  (let [f (fn [xs x] (contains? xs x))
        g (fn [xs x] ( = x (first xs)))
        z (some-fn f g)]
    (z [1 2 3] 1)))
(defn funcion-some-fn-17
  []
  (let [f (fn [xs] (every? number? xs))
        g (fn [xs] (every? char? xs))
        z (some-fn g f)]
    (z [2 3])))
(defn funcion-some-fn-18
  []
  (let [f (fn [xs] (every? pos? xs))
        g (fn [xs] (every? int? xs))
        z (some-fn f g)]
    (z [1])))
(defn funcion-some-fn-19
  []
  (let [f (fn [xs] (not-every? even? xs))
        g (fn [xs] (not-every? odd? xs))
        z (some-fn g f)]
    (z '(1 2 3 4))))
(defn funcion-some-fn-20
  []
  (let [f (fn [xs] (not-every? string? xs))
        g (fn [xs] (not-every? symbol? xs ))
        z (some-fn g f)]
    (z ["abc"])))
 
(funcion-some-fn-1)
(funcion-some-fn-2)
(funcion-some-fn-3)
(funcion-some-fn-4)
(funcion-some-fn-5)
(funcion-some-fn-6)
(funcion-some-fn-7)
(funcion-some-fn-8)
(funcion-some-fn-9)
(funcion-some-fn-10)
(funcion-some-fn-11)
(funcion-some-fn-12)
(funcion-some-fn-13)
(funcion-some-fn-14)
(funcion-some-fn-15)
(funcion-some-fn-16)
(funcion-some-fn-17)
(funcion-some-fn-18)
(funcion-some-fn-19)
(funcion-some-fn-20)









































